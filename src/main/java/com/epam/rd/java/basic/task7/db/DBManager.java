package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            return new DBManager();
        }
        return instance;
    }

    private DBManager() {
        try {
            con = getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection con;

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL);
    }

    // //////////////////////////////////////
    // SQL Queries
    // //////////////////////////////////////

    private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users ORDER BY id";
    private static final String SQL_FIND_USER = "SELECT * FROM users WHERE login=?";
    private static final String SQL_INSERT_USER = "INSERT INTO users (login) values (?)";
    private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
    private static final String SQL_FIND_TEAM = "SELECT * FROM teams WHERE name=?";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams (name) values (?)";
    private static final String SQL_SET_TEAM_FOR_USER = "INSERT INTO users_teams (user_id, team_id) values ";
    private static final String SQL_FIND_USER_TEAMS = "SELECT * FROM users_teams WHERE user_id=?";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE id=?";
    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE id=?";
    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";
    private static final String SQL_DELETE_TEAM_CASCADE = "DELETE FROM users_teams WHERE team_id=?";
    private static final String SQL_DELETE_USER_CASCADE = "DELETE FROM users_teams WHERE user_id=?";

    // //////////////////////////////////////
    // Methods
    // //////////////////////////////////////

    public List<User> findAllUsers() throws DBException {
        List<User> allUser = new ArrayList<>();

        try (Statement statement = con.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_USERS)) {
            addUser(allUser, resultSet);
        } catch (SQLException e) {
            throwException(e);
        }

        return allUser;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> allTeam = new ArrayList<>();

        try (Statement statement = con.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_TEAMS)) {
            addTeam(allTeam, resultSet);
        } catch (SQLException e) {
            throwException(e);
        }

        return allTeam;
    }

    public boolean insertUser(User user) throws DBException {
        return !userIsExist(user) && insert(user);
    }

    public boolean insertTeam(Team team) throws DBException {
        return !teamIsExist(team) && insert(team);
    }

    public User getUser(String login) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_FIND_USER);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return User.createUser(resultSet.getInt("id"), resultSet.getString("login"));
            }

        } catch (SQLException e) {
            throwException(e);
        }
        return User.createUser(login);
    }

    public Team getTeam(String name) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_FIND_TEAM);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return Team.createTeam(resultSet.getInt("id"), resultSet.getString("name"));
            }

        } catch (SQLException e) {
            throwException(e);
        }
        return Team.createTeam(name);
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> userTeams = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_FIND_USER_TEAMS);
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int team_id = resultSet.getInt("team_id");
                userTeams.add(findTeamById(team_id));
            }

        } catch (SQLException e) {
            throwException(e);
        }
        return userTeams;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        return userIsExist(user) && teamsAreExist(teams) && setUserForEachTeam(user, teams);
    }

    public boolean deleteTeam(Team team) throws DBException {
        return teamIsExist(team) && delete(team) && deleteOnCascade(team);
    }

    public boolean updateTeam(Team team) throws DBException {
        return update(team);
    }


    public boolean deleteUsers(User... users) throws DBException {
        if (userAreExist(users)) {
            for (User user : users) {
                deleteUser(user);
            }
            return true;
        }
        return false;
    }

    // //////////////////////////////////////
    // Private Methods
    // //////////////////////////////////////

    private void addUser(List<User> allUser, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            User user = User.createUser();

            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));

            allUser.add(user);
        }
    }

    private void addTeam(List<Team> allTeam, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            Team team = Team.createTeam();

            team.setId(resultSet.getInt("id"));
            team.setName(resultSet.getString("name"));

            allTeam.add(team);
        }
    }

    private boolean userIsExist(User newUser) throws DBException {
        return findAllUsers().stream().anyMatch(user -> user.equals(newUser));
    }

    private boolean teamIsExist(Team newTeam) throws DBException {
        return findAllTeams().stream().anyMatch(team -> team.equals(newTeam));
    }

    private boolean userAreExist(User[] users) {
        return Arrays.stream(users).allMatch(user -> {
            try {
                return userIsExist(user);
            } catch (DBException e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    private boolean teamsAreExist(Team[] teams) {
        return Arrays.stream(teams).allMatch(team -> {
            try {
                return teamIsExist(team);
            } catch (DBException e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    private boolean insert(User user) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getLogin());
            settingId(user, preparedStatement, preparedStatement.executeUpdate());
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private boolean insert(Team team) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, team.getName());
            settingId(team, preparedStatement, preparedStatement.executeUpdate());
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private boolean update(Team team) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_UPDATE_TEAM);

            int k = 0;
            preparedStatement.setString(++k, team.getName());
            preparedStatement.setInt(++k, team.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private boolean delete(Team team) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_DELETE_TEAM);
            preparedStatement.setInt(1, team.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private boolean deleteOnCascade(Team team) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_DELETE_TEAM_CASCADE);
            preparedStatement.setInt(1, team.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private boolean delete(User user) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_DELETE_USER);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private boolean deleteOnCascade(User user) throws DBException {
        try {
            PreparedStatement preparedStatement = con.prepareStatement(SQL_DELETE_USER_CASCADE);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private boolean deleteUser(User user) throws DBException {
        return delete(user) && deleteOnCascade(user);
    }

    private void settingId(Object obj, PreparedStatement preparedStatement, int count) throws SQLException {
        if (count > 0) {
            if (obj.getClass() == Team.class) {
                Team team = (Team) obj;
                try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        team.setId(resultSet.getInt(1));
                    }
                }
            }

            if (obj.getClass() == User.class) {
                User user = (User) obj;
                try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        user.setId(resultSet.getInt(1));
                    }
                }
            }

        }
    }

    private boolean setUserForEachTeam(User user, Team[] teams) throws DBException {
        try {
            Statement statement = con.createStatement();
            StringBuilder new_SQL_SET_TEAM_FOR_USER = new StringBuilder(SQL_SET_TEAM_FOR_USER);
            for (int i = 0; i < teams.length; i++) {
                new_SQL_SET_TEAM_FOR_USER.append("(");
                new_SQL_SET_TEAM_FOR_USER.append(user.getId());
                new_SQL_SET_TEAM_FOR_USER.append(", ");
                new_SQL_SET_TEAM_FOR_USER.append(teams[i].getId());
                new_SQL_SET_TEAM_FOR_USER.append(")");

                if(i+1 == teams.length) {
                    break;
                }
                new_SQL_SET_TEAM_FOR_USER.append(", ");
            }
            statement.executeUpdate(new_SQL_SET_TEAM_FOR_USER.toString());
        } catch (SQLException e) {
            throwException(e);
        }
        return true;
    }

    private Team findTeamById(int id) throws DBException {
        return findAllTeams().stream().filter(team -> team.getId() == id).collect(Collectors.toList()).get(0);
    }

    private void throwException(Exception e) throws DBException {
        throw new DBException(e.getMessage(), e);
    }

}
