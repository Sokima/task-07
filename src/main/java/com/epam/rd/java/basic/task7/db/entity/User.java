package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	private User() {

	}

	private User(String login) {
		this.login = login;
	}

	private User(int id, String login) {
		this.id = id;
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser() {
		return new User();
	}

	public static User createUser(String login) {
		return new User(login);
	}

	public static User createUser(int id, String login) {
		return new User(id, login);
	}

	@Override
	public String toString() {
		return "User[" + "login=" + login + ']';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		User user = (User) o;
		return Objects.equals(this.login, user.login);
	}

}
